
'''
 ' usage: python burning-ship.py w h x0 y0 x1 y1 fileName
 '
 ' where:
 ' 
 '    w = width of output image
 '    h = height of output image
 '    x0 = x coordinate of upper left corner of fractal
 '    y0 = y     "      "    "     "     "    "    "
 '    x1 = x     "      "  lower right   "    "    "
 '    y1 = y     "      "    "     "     "    "    "
 '    fileName = name of file to save fractal in
'''
import imageio
from mpi4py import MPI
import numpy as np
import sys

'''
 ' Global tag constants
'''
FRACTAL_COORDS = 1
IMAGE_BOUNDS = 2
IMAGE_HEIGHT = 3
STRIPE = 4

'''
 ' Map a value from the range [valLo, valHi] to [outLo, outHi].
 '
 ' param val Value to map
 ' param valLo Lowest value val could have
 ' param valHi Highest value val could have
 ' param outLo Lowest value output could have
 ' param outHi Higest value output could have
'''
def mapToRange(val, valLo, valHi, outLo, outHi):
    return outLo + ((outHi - outLo) / (valHi - valLo)) * (val - valLo)

'''
 ' Test a point to see if it belongs to the Mandelbrot set or not.
'''
def testPoint(x, y):
    a = 0.0
    b = 0.0
    ta = 0.0
    i = 0

    while i < 255:
        a = abs(a)
        b = abs(b)
        ta = a**2 - b**2 + x
        b = 2 * a * b + y
        a = ta
        i += 1
        
        if abs(a) > 2.0 or abs(b) > 2.0:
            return i

        i += 1

    return i

'''
 ' Main program starts here.
'''

# get the communications object
comm = MPI.COMM_WORLD

# get total number of processors
size = comm.Get_size()

# get rank of current processor
rank = comm.Get_rank()

# get name of the processor
name = MPI.Get_processor_name()

# master process
if rank == 0:
    # save command-line arguments
    w = int(sys.argv[1])
    h = int(sys.argv[2])
    x0 = float(sys.argv[3])
    y0 = float(sys.argv[4])
    x1 = float(sys.argv[5])
    y1 = float(sys.argv[6])
    fileName = sys.argv[7]

    # calculate stripe bounds
    stripeBounds = []
    stripeWidth = w // size
    w0 = 0
    w1 = 0
    for i in range(1, size - 1):
        # FILL IN CODE HERE
        print(i) # replace this line

    # create a blank, w x h pixel image
    image = np.zeros((h, w, 3), dtype = 'uint8')

    # send information to the slaves
    for (i, wb) in enumerate(stripeBounds):
        # FILL IN CODE HERE
        print(i, wb) # replace this line

    # receive information from the slaves
    #for i in range(1, size):
        # get the stripe
        #stripe = comm.recv(source = i, tag = STRIPE)

        # put the stripe into the main image
        #image[:, stripeBounds[i - 1][0]:stripeBounds[i - 1][1] + 1, :] = stripe

    # finish up by writing the image
    #imageio.imwrite(fileName, image)

# slave process
else:
    # receive stripe information
    # FILL IN CODE HERE
    widthBounds = [10, 20, 30] # replace this line
    fracCoords = [-2, -2, 0.5, 1.5] # replace this line
    h = 40 # replace this line

    # create NumPy array for this stripe
    w = widthBounds[1] - widthBounds[0] + 1
    image = np.zeros((h, w, 3), dtype = 'uint8')
    for row in range(h):
        for col in range(w):
            x = mapToRange(widthBounds[0] + col, 0, widthBounds[2] - 1, fracCoords[0], fracCoords[2])
            y = mapToRange(row, 0, h - 1, fracCoords[1], fracCoords[3])

            c = testPoint(x, y)

            image[row][col] = [c, c, c]
    
    # send stripe data back to the master process
    # FILL IN CODE HERE